package com.example.myapplicationbundle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MainActivity2.class);
                Bundle bundle = new Bundle();
                bundle.putString("chuoi", "Đại học Sư phạm Kĩ thuật");
                bundle.putInt("tuoi", 20);
                bundle.putString("ten", "Hồ Quang Phúc");
                bundle.putString("lop", "20T2");
                bundle.putString("email", "2050531200250@sv.ute.udn.vn");
                i.putExtra("data", bundle);
                startActivity(i);
            }
        });

    }
}
package com.example.myapplicationbundle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        TextView textView = (TextView) findViewById(R.id.textView3);
        Intent i = getIntent();
        Bundle bundle = i.getBundleExtra("data");
        String truong = bundle.getString("chuoi");
        String ten = bundle.getString("ten");
        String lop = bundle.getString("lop");
        String email = bundle.getString("email");
        textView.setText(truong + "\n" + lop + "\n" + ten  + "\n" + email );
    }
}